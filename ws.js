const userData = require('./data/users.json');
const uuidv4 = require('uuid/v4');

var WebSocketServer = require('ws').Server,
  wss = new WebSocketServer({port: 40510})

wss.on('connection', function (ws) {
  ws.on('message', function (message) {
    console.log('received: %s', message)
  })


  setInterval(
    () => {
      let i = Math.floor(Math.random() * (1 + userData.length));
      let msg = {
        id : uuidv4(),
        user : userData[i],
        date : new Date()
      }
      ws.send(JSON.stringify(msg))
    },
    5000
  )
})
